import React, {useEffect} from 'react'
import {fetchList} from '../actions/index.js'
import {connect} from "react-redux"
import Item from "./Item"
import Menu from "./Menu"

 function List({fetchList,listdata}) {

    useEffect(() => {
      fetchList()
    },[])


    if(listdata.length==0){
        return <div>loading..</div>
    }



    return (
        <div>
            <h2>Contact List</h2>
             <Menu />
            <div className="ui celled list">
                {listdata.map((item)=>{
                   return <Item item={item} key={item.id}/>
                })}
                   
            </div>
           
        </div>
    )
}


const mapStateToProps = (state)=>{
     const {listdata} = state
    return {listdata};
  }
  
  export default connect(mapStateToProps,{fetchList}) (List);
