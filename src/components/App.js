import React, { useEffect } from "react";
// components
import Navbar from "./NavBar";
import List from "./List";
import reducer from "../reducers"
import { Provider } from "react-redux"
import reduxThunk from 'redux-thunk'
import { Router, Route, Switch } from 'react-router-dom'
import history from '../history.js'
import UserDetail from '../components/Detail'
import FavouriteList from '../components/UserFavouriteList'
import { createStore, applyMiddleware, compose } from "redux"

//  const store = createStore(reducer)   //used to pass reducer function
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(reduxThunk)
));


function App() {
  // list setup

  return (
    <div className="ui container">
      <Provider store={store}>
        <Router history={history}>
          <Navbar />
         
          <Switch>
            <Route path='/' exact={true} component={List} />
            <Route path='/user/:id' component={UserDetail} />
            <Route path='/favourite'  component={FavouriteList} />

          </Switch>
         
        </Router>

      </Provider>
    </div>
  );
}

export default App;
