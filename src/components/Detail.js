import React, { useEffect, useState } from 'react'
import { fetchUser, setUserFavorite,removeUserFavorite } from '../actions'
import { connect } from 'react-redux'



const initialFav = localStorage.getItem("user_fav") ? JSON.parse(localStorage.getItem("user_fav")) : [];

function Detail({ user, id, fetchUser,fav_status, setUserFavorite,removeUserFavorite }) {

    useEffect(() => {
        fetchUser(id);
    }, [])


    let handleAddFav = (user) => {
        setUserFavorite(user)
    }

    //handle delete
    const handleDeleteFav = (id) => {
        removeUserFavorite(id);
    }

    if (user.length == 0) {
        return <div>loading...</div>
    }

    return (
        <div>
        <h2>My Detail</h2>
        <div className="ui link cards">
        
            <div className="card">
                <div className="image">
                    <img src={user.avatar} />
                </div>
                <div className="content">
                    <div className="header">{user.first_name} {user.last_name}</div>
                    <div className="meta">
                        <a>Email</a>
                    </div>
                    <div className="description">
                        {user.email}
                    </div>
                </div>
                <div className="extra content">
                    <button type="button" className="ui toggle button" onClick={ fav_status ? ()=>handleDeleteFav(id) :()=>handleAddFav(user)}>
                        {fav_status ? 'remove favourite':'Add to favourite'} 
                   </button>
                </div>
            </div>
        </div>
        </div>
    )
}


const mapStateToProps = (state, ownProps) => {
    
    let fav_status=false;
    if(state.favourite.length>0){
        let editfavourite = state.favourite.find(item => item.id === state.user.id)
        fav_status= editfavourite ? true:false

    }
   
    return {
        user: state.user,
        id: ownProps.match.params.id,
        fav_status
    }
}

export default connect(mapStateToProps, { fetchUser, setUserFavorite,removeUserFavorite })(Detail)