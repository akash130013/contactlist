import React from 'react'
import { Link } from 'react-router-dom'

export default function Item({ item }) {
    return (
        <div className="item" key={item.id}>
            <div className="large middle icon aligned camera">
            <img className="ui avatar image" src={item.avatar} />
            <div className="content">
                <Link to={`user/${item.id}`} className="header">{item.first_name} {item.last_name}</Link>
            </div>
            </div>
        </div>
    )
}
