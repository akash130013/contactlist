import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import {sorting} from '../actions'

 function Menu({menuOption,sorting}) {

    const menuArr=['asc','desc'];

    useEffect(()=>{
       sorting(menuOption)
    },[])

    let  findOption=(e)=>{
        
       sorting(e.target.value)
        
    }

    return (
        <>
                <select value={menuOption} onChange={findOption}>
                    <option value="DEFAULT">Choose a option ...</option>
                    {

                        menuArr ? menuArr.map((item, i) => {
                           
                            return <option value={item} key={i}>{item}</option>
                        }) : null
                    }
                </select>
        </>
    )
}


const mapStateToProps = (state) => {
    return {
        menuOption:state.menuOption,
    }
}

export default connect(mapStateToProps, { sorting })(Menu)
