import { FETCH_LIST, FETCH_USER, SORTING, FETCH_FAVOURITE, ADD_FAVOURITE, REMOVE_FAVOURITE } from "../actions/type.js"
// items
import * as _ from "lodash";
import { reverse } from "lodash";



const initialFav = localStorage.getItem("user_fav") ? JSON.parse(localStorage.getItem("user_fav")) : [];

const initialStore = {
    listdata: [],
    user: [],
    menuOption: 'all',
    favourite: initialFav,
}

function reducer(state = initialStore, action) {

    switch (action.type) {


        case FETCH_LIST:

            const data = sorting(action.payload.data, state.menuOption)

            return { ...state, listdata: data }

        case FETCH_USER:

            return { ...state, user: action.payload.data }
        case SORTING:

            let sorteddata = sorting(state.listdata, action.sort)

            return { ...state, listdata: sorteddata, menuOption: action.sort }
       
        case ADD_FAVOURITE:

            // initialFav.push(action.userFav);
            let newArr=state.favourite.concat(action.userFav);
            // let user=[state.favourite]:action.userFav
            localStorage.setItem("user_fav", JSON.stringify(newArr))

            return { ...state, favourite:newArr};

        case REMOVE_FAVOURITE:

            const newdata = state.favourite.filter((item) => {
                return item.id !== parseInt(action.id);
            })

            localStorage.clear();
            localStorage.setItem("user_fav", JSON.stringify(newdata));
            return { ...state, favourite: newdata }

        default:
            return state;

    }

}


const sorting = (sorteddata, sort) => {

    switch (sort) {
        case 'asc':

            return _.sortBy(sorteddata, 'first_name')

        case 'desc':

            sorteddata = _.sortBy(sorteddata, 'first_name');
            return sorteddata.reverse();

        default:
            return sorteddata
    }

}


export default reducer